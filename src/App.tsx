import React, {Component} from 'react';
import './App.css';
import CounterBody from "./components/CounterBody";
import SideBar from "./components/SideBar";


type Cards = {
    name: string,
    currentCount: number
}

export type CardHolder = {
    name: string,
    cards: Array<Cards>
}

type State = {
    allCards: Array<CardHolder>,
    currentCardsIndex: number
}

type Props = {}

class App extends Component <Props, State> {

    getCardsFromSessionOrDefault = () => {
        let item = localStorage.getItem("cards");
        if (item != null) {
            return JSON.parse(item)
        }

        return [{name: "default", cards: []}];
    };

    state: Readonly<State> = {
        allCards: this.getCardsFromSessionOrDefault(),
        currentCardsIndex: 0
    };

    handleNameInputChange = (newName: string) => {
        let newCardHolder = this.state.allCards.slice();
        newCardHolder[this.state.currentCardsIndex].name = newName;

        this.setState({
            allCards: newCardHolder
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })
    };

    destroyCard = (key: number) => {
        let newCards = this.state.allCards.slice();
        newCards[this.state.currentCardsIndex].cards.splice(key, 1);

        this.setState({
            allCards: newCards
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })

    };

    addCard = (name: string) => {
        let newCards = this.state.allCards.slice();
        newCards[this.state.currentCardsIndex].cards.push({name, currentCount: 0});

        this.setState({
            allCards: newCards
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })
    };

    updateCard = (key: number, valueChange: number) => {
        let newCards = this.state.allCards.slice();
        newCards[this.state.currentCardsIndex].cards[key].currentCount += valueChange;

        this.setState({
            allCards: newCards
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })
    };

    displaySectionCallback = (key: number) => {
        this.setState({
            currentCardsIndex: key
        })
    };

    getJustNames = (): Array<string> => {
        return this.state.allCards.map(card => card.name)
    };

    addOrganizationCard = (): void => {
        const newCardHolder: CardHolder = {
            name: "",
            cards: new Array<Cards>()
        };

        const currentCardArray = this.state.allCards.slice();
        currentCardArray.push(newCardHolder);


        this.setState({
            ...this.state,
            allCards: currentCardArray
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })
    };

    removeOrganizationCard = (index: number): void => {
        this.setState({
            ...this.state,
            allCards: this.state.allCards.filter((item, i) => {
                return i !== index
            })
        }, () => {
            this.updateLocalStorage(this.state.allCards)
        })
    };

    updateLocalStorage = (cards: Array<CardHolder>) => {
        localStorage.setItem("cards", JSON.stringify(cards))
    };

    render() {
        return (
            <div className="App global-grid">
                <section className={"deck-body"}>
                    <SideBar addCard={this.addOrganizationCard} names={this.getJustNames()}
                             displaySectionCallback={this.displaySectionCallback}
                             handleNameInputChange={this.handleNameInputChange}
                             currentlySelectedNumber={this.state.currentCardsIndex}
                             destroy={this.removeOrganizationCard}/>
                </section>
                <section className={"card-body"}>
                    <CounterBody holder={this.state.allCards[this.state.currentCardsIndex]}
                                 addCard={this.addCard}
                                 destroyCard={this.destroyCard} updateCard={this.updateCard}/>
                </section>
            </div>
        );
    }

}

export default App;
