import ReactDOM from "react-dom";
import React from "react";
import SideBar from "../../components/SideBar";

describe('SideBarTests', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SideBar displaySectionCallback={jest.fn()} handleNameInputChange={jest.fn()}
                                 currentlySelectedNumber={0} names={["hi"]}
                                 destroy={jest.fn()} addCard={jest.fn()}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    })
});
