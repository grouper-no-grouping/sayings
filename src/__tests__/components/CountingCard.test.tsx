import React from "react";
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CountingCard from "../../components/CountingCard";

Enzyme.configure({adapter: new Adapter() });


it('Initial value set properly', () => {
    const mockFn = jest.fn();
    const component = shallow(<CountingCard count={0} counting={"counting"} cardKey={0} handleClick={mockFn}/>);

    expect(component.find(".card-count").text()).toEqual("0");

    expect(component).toMatchSnapshot()
});


it('Button causes change', () => {
    const mockFn = jest.fn();
    const component = shallow(<CountingCard count={0} counting={"counting"} cardKey={0} handleClick={mockFn}/>);

    component.find(".card-button").at(0).simulate('click');
    expect(mockFn).toHaveBeenCalledWith(0, 1);

    component.find(".card-button").at(1).simulate('click');
    expect(mockFn).toHaveBeenCalledWith(0, -1);
});
