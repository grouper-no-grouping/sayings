import React, {Component} from 'react';

type Props = {
    count: number,
    counting: string,
    cardKey: number,
    handleClick(key: number, valueChange: number): void,
    destroyCardCallback(key: number): void
}


enum RelevantKeyCodes {
    ESCAPE = 27,
    UP_KEY = 38,
    DOWN_KEY = 40
}

class CountingCard extends Component<Props> {

    constructor(props: Props) {
        super(props);

        this.handleEntireCardInteraction = this.handleEntireCardInteraction.bind(this)
    }


    handleEntireCardInteraction(e: React.KeyboardEvent<HTMLDivElement>) {
        //TODO: Handle focus After Delete
        switch (e.keyCode) {
            case RelevantKeyCodes.ESCAPE:
                this.props.destroyCardCallback(this.props.cardKey);
                break;
            case RelevantKeyCodes.UP_KEY:
                this.props.handleClick(this.props.cardKey, 1);
                break;
            case RelevantKeyCodes.DOWN_KEY:
                this.props.handleClick(this.props.cardKey, -1);
        }
    }

    render() {
        return (
            <div className="card" tabIndex={0} onKeyDown={this.handleEntireCardInteraction}>
                <div className="inner-card-grid">
                    <button className="card-escape-button soft-border right-self" onClick={() => {
                        this.props.destroyCardCallback(this.props.cardKey)
                    }}>x
                    </button>
                    <div className="card-text center-self">
                        {this.props.counting}
                    </div>
                    <div className="card-count center-self soft-border">
                        {this.props.count}
                    </div>
                    <div className="card-counter">
                        <button className="card-button center-self soft-border" onClick={() => {
                            this.props.handleClick(this.props.cardKey, 1)
                        }}> + </button>
                        <button className="card-button center-self soft-border" onClick={() => {
                            this.props.handleClick(this.props.cardKey, -1)
                        }}> - </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default CountingCard;


