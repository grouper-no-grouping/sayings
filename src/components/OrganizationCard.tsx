import React, {ChangeEvent, Component} from 'react';
import {RelevantKeyCodes} from "./common/RelevantKeyCodes";


type Props = {
    name: string,
    index: number,
    clickKey: number,
    clickOfOrganizationCard(key: number): void,
    currentlySelected: boolean,
    handleNameInputChange(newName: string): void,
    destroyCardCallback(index: number): void
}

class OrganizationCard extends Component<Props> {

    handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.props.handleNameInputChange(e.target.value);
    };

    handleEntireCardInteraction = (e: React.KeyboardEvent<HTMLDivElement>): void => {
        if (e.keyCode === RelevantKeyCodes.ESCAPE) {
            this.props.destroyCardCallback(this.props.index);
        }
    };

    render() {
        return (
            <div className={"card".concat((this.props.currentlySelected) ? " card-selected" : "")} tabIndex={0}
                 onKeyDown={this.handleEntireCardInteraction}>
                <div className="inner-org-card-grid" onClick={() => {
                    this.props.clickOfOrganizationCard(this.props.clickKey)
                }}>
                    <div className="center-self">
                        <input value={this.props.name} onChange={this.handleInputChange}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default OrganizationCard;
