import React, {ChangeEvent, Component} from 'react';

type Props = {
    addCardCallback(valueOfWhatToCount: string): void,
}

type State = {
    currentInputValue: string,
    errorInput: boolean
}

class AddingCard extends Component<Props, State> {
    state: Readonly<State> = {
        currentInputValue: "",
        errorInput: false
    };

    handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({
            currentInputValue: e.target.value
        });
    };

    handleCardSubmission = () => {
        if (this.state.currentInputValue === "") {
            this.setState({
                ...this.state,
                errorInput: true
            });
            return;
        } else {
            this.props.addCardCallback(this.state.currentInputValue);
            this.setState({
                currentInputValue: '',
                errorInput: false
            })
        }
    };

    handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            this.handleCardSubmission()
        }
    };

    render() {
        return (
            <div className="card" tabIndex={0}>
                <div className="adder-card-grid">
                    <div className="card-text center-self">
                        What to count next?
                    </div>
                    <div>
                        <input onKeyDown={this.handleKeyDown}
                                  className={`card-add-input ${(this.state.errorInput) ? 'error-text-input' : ''}`}
                                  value={this.state.currentInputValue} placeholder="What to count next?"
                                  onChange={this.handleInputChange}/>
                        <div className="hidden error">Uh-oh, we got an error over here.</div>
                    </div>
                    <div className="card-adder">
                        <button className="card-text-button center-self soft-border"
                                onClick={this.handleCardSubmission}>
                            Start Counting
                        </button>
                    </div>
                </div>

            </div>
        );
    }

}

export default AddingCard;
